<?php
/**
 * @file
 * 3Dmol.js field formatter.
 */

/**
 * Implements hook_field_formatter_info().
 *
 * Provide a formatter for 3Dmol.js files.
 */
function field_3dmoljs_field_formatter_info() {
  $formatters = array();
  $formatters['field_3dmoljs_file'] = array(
    'label' => t('3Dmol.js File'),
    'field types' => array('3dmoljs_file'),
    'settings' => array(
      'preview_style' => 'none',
      'display_style' => 'inline',
      'autoplay' => 'inline',
      'popup_height' => 'inline',
      'popup_width' => 'inline',
    ),
  );
  return $formatters;
}

/**
 * Implements hook_field_formatter_view().
 */
function field_3dmoljs_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $element = array();
  $settings = $display['settings'];

  foreach ($items as $delta => $item) {
    $element[$delta] = array(
      '#markup' => theme('field_3dmoljs', array(
        'file' => $item['uri'],
      )),
    );
  }
  return $element;
}

/**
 * Implements hook_field_formatter_settings_form().
 */
//function field_3dmoljs_field_formatter_settings_form($field, $instance, $view_mode, $form, &$form_state) {
//  $settings = $instance['display'][$view_mode]['settings'];
//  $element = array();
//  return $element;
//}

/**
 * Implements hook_field_formatter_settings_summary().
 */
//function field_3dmoljs_field_formatter_settings_summary($field, $instance, $view_mode) {
//  $settings = $instance['display'][$view_mode]['settings'];
//  $styles = array('none' => 'original');
//  if (module_exists('image')) {
//    foreach (image_styles() as $key => $style) {
//      $styles[$key] = $style['name'];
//    }
//  }
//
//  $summary = t('Preview as %style image and play %display.', array(
//    '%style' => $styles[$settings['preview_style']],
//    '%display' => $displays[$settings['display_style']],
//  ));
//  return $summary;
//}
