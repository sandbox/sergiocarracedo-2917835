<?php
/**
 * @file
 * 3Dmol.js player field widget.
 */

/**
 * Implements hook_widget_info().
 */
function field_3dmoljs_field_widget_info() {
  return array(
    '3dmoljs_file' => array(
      'label' => t('3Dmol.js viewer'),
      'field types' => array('3dmoljs_file'),
      'settings' => array(
        'progress_indicator' => 'throbber',
      ),
      'behaviors' => array(
        'multiple values' => FIELD_BEHAVIOR_CUSTOM,
        'default value' => FIELD_BEHAVIOR_NONE,
      ),
    ),
  );
}

/**
 * Implements hook_widget_settings_form().
 */
function field_3dmoljs_widget_settings_form($field, $instance) {
  return file_field_widget_settings_form($field, $instance);
}

/**
 * Implements hook_field_widget_form().
 */
function field_3dmoljs_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {
  // Add display_field setting to field because file_field_widget_form() assumes it is set.
  $field['settings']['display_field'] = 0;
  $field['settings']['file_directory'] = 'public://';
  return file_field_widget_form($form, $form_state, $field, $instance, $langcode, $items, $delta, $element);
}
