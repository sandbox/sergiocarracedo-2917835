<?php
/**
 * @file
 * Implement an 3dMol.js field, based on the file module's file field.
 */

/**
 * Implements hook_field_info().
 */
function field_3dmoljs_field_info() {
  return array(
    '3dmoljs_file' => array(
      'label' => t('3Dmol.js file'),
      'description' => t('An 3Dmol.js compatible file.'),
      'settings' => array(
        'uri_scheme' => variable_get('file_default_scheme', 'public'),
      ),
      'instance_settings' => array(
        'file_extensions' => 'gro',
        'file_directory' => '',
      ),
      'default_widget' => 'field_3dmoljs_file',
      'default_formatter' => 'field_3dmoljs_file',
    ),
  );
}

/**
 * Implements hook_field_load().
 */
function field_3dmoljs_field_load($entity_type, $entities, $field, $instances, $langcode, &$items, $age) {
  file_field_load($entity_type, $entities, $field, $instances, $langcode, $items, $age);
}

/**
 * Implements hook_field_insert().
 */
function field_3dmoljs_field_insert($entity_type, $entity, $field, $instance, $langcode, &$items) {
  file_field_insert($entity_type, $entity, $field, $instance, $langcode, $items);
}

/**
 * Implements hook_field_update().
 */
function field_3dmoljs_field_update($entity_type, $entity, $field, $instance, $langcode, &$items) {
  file_field_update($entity_type, $entity, $field, $instance, $langcode, $items);
}

/**
 * Implements hook_field_delete().
 */
function field_3dmoljs_field_delete($entity_type, $entity, $field, $instance, $langcode, &$items) {
  file_field_delete($entity_type, $entity, $field, $instance, $langcode, $items);
}

/**
 * Implements hook_field_delete_revision().
 */
function field_3dmoljs_field_delete_revision($entity_type, $entity, $field, $instance, $langcode, &$items) {
  file_field_delete_revision($entity_type, $entity, $field, $instance, $langcode, $items);
}

/**
 * Implements hook_field_is_empty().
 */
function field_3dmoljs_field_is_empty($item, $field) {
  return file_field_is_empty($item, $field);
}
