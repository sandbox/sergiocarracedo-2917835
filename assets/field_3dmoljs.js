/**
 * @file
 * Drupal behavior to run 3Dmol.js player plugin.
 */
(function($, Drupal){
  // 3dmoljs player behavior.
  Drupal.behaviors.field_3dmoljs_player = {
    attach: function(context, settings) {

      $('.field-type-3dmoljs-file .field-3dmoljs-player', context).each(function() {
        var $el = $(this);

        $.ajax($el.data('file'), {
          success: function (data) {
            var config = {};
            var viewer = $3Dmol.createViewer($el, config);
            viewer.addModel(data, "gro");
            viewer.setStyle({},{sphere:{radius:.1,}});
            /*viewer.addSphere({
              center: {x: 0, y: 0, z: 0},
              radius: 10.0,
              color: 'green'
            });*/
            viewer.setClickable({}, true, function(e) {
              console.log(e);
            })
            viewer.zoomTo();
            viewer.render();
            viewer.zoom(0.8, 2000);
          }
        })
      })
    }
  };
}(jQuery, Drupal));
